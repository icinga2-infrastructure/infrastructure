
# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592
#         6442450944

servers = {

    database = {
        memory    = 2048
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 10
        hostname  = "database-1"
    }

    tsdb = {
        memory    = 1024
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 12
        hostname  = "tsdb-1"
    }

    icingaweb = {
        memory    = 1024
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 15
        hostname  = "icingaweb-1"
    }

    icinga_master_1 = {
        memory    = 1024
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 20
        hostname  = "master-1"
    }

    icinga_master_2 = {
        memory    = 1024
        vcpu      = 1
        disk_size = "6442450944"
        octetIP   = 21
        hostname  = "master-2"
    }

}
